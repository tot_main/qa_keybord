//
//  AppDelegate.h
//  qa_test_keyBord
//
//  Created by totsukatakumi on 2014/09/26.
//  Copyright (c) 2014年 戸塚　巧. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

