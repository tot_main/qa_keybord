//
//  main.m
//  qa_test_keyBord
//
//  Created by totsukatakumi on 2014/09/26.
//  Copyright (c) 2014年 戸塚　巧. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
