//
//  KeyboardViewController.m
//  QA_KeyBord
//
//  Created by totsukatakumi on 2014/09/26.
//  Copyright (c) 2014年 戸塚　巧. All rights reserved.
//

#import "KeyboardViewController.h"

@interface KeyboardViewController ()
@property (nonatomic, strong) UIButton *nextKeyboardButton;
@property (nonatomic, assign) NSInteger stringCount;
@property (nonatomic, strong) UILabel * nowStringCountLabel;
@end

@implementation KeyboardViewController

- (void)updateViewConstraints {
    [super updateViewConstraints];
    
    // Add custom view sizing constraints here
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Perform custom UI setup here
    self.nextKeyboardButton = [UIButton buttonWithType:UIButtonTypeSystem];
    
    [self.nextKeyboardButton setTitle:NSLocalizedString(@"Next Keyboard", @"Title for 'Next Keyboard' button") forState:UIControlStateNormal];
    [self.nextKeyboardButton sizeToFit];
    self.nextKeyboardButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.nextKeyboardButton addTarget:self action:@selector(advanceToNextInputMode) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.nextKeyboardButton];
    
    NSLayoutConstraint *nextKeyboardButtonLeftSideConstraint = [NSLayoutConstraint constraintWithItem:self.nextKeyboardButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0];
    NSLayoutConstraint *nextKeyboardButtonBottomConstraint = [NSLayoutConstraint constraintWithItem:self.nextKeyboardButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    [self.view addConstraints:@[nextKeyboardButtonLeftSideConstraint, nextKeyboardButtonBottomConstraint]];
	
	// Create KeyBord
	[self createKeyBord];
}

-(void)createKeyBord {
	_nowStringCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300, 20)];
	[self.view addSubview:_nowStringCountLabel];
	
	UIButton * num1000add = [UIButton buttonWithType:UIButtonTypeSystem];
	[num1000add setFrame:CGRectMake(10, 40, 50, 20)];
	[num1000add setTitle:@"+1000" forState:UIControlStateNormal];
	[num1000add setTag:10];
	[num1000add addTarget:self action:@selector(stringCountChange:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:num1000add];
	
	UIButton * num1000del = [UIButton buttonWithType:UIButtonTypeSystem];
	[num1000del setFrame:CGRectMake(10, 70, 50, 20)];
	[num1000del setTitle:@"-1000" forState:UIControlStateNormal];
	[num1000del setTag:11];
	[num1000del addTarget:self action:@selector(stringCountChange:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:num1000del];
	
	
	UIButton * num100add = [UIButton buttonWithType:UIButtonTypeSystem];
	[num100add setFrame:CGRectMake(70, 40, 50, 20)];
	[num100add setTitle:@"+100" forState:UIControlStateNormal];
	[num100add setTag:12];
	[num100add addTarget:self action:@selector(stringCountChange:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:num100add];
	
	UIButton * num100del = [UIButton buttonWithType:UIButtonTypeSystem];
	[num100del setFrame:CGRectMake(70, 70, 50, 20)];
	[num100del setTitle:@"-100" forState:UIControlStateNormal];
	[num100del setTag:13];
	[num100del addTarget:self action:@selector(stringCountChange:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:num100del];
	
	
	
	UIButton * num10add = [UIButton buttonWithType:UIButtonTypeSystem];
	[num10add setFrame:CGRectMake(130, 40, 50, 20)];
	[num10add setTitle:@"+10" forState:UIControlStateNormal];
	[num10add setTag:14];
	[num10add addTarget:self action:@selector(stringCountChange:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:num10add];
	
	UIButton * num10del = [UIButton buttonWithType:UIButtonTypeSystem];
	[num10del setFrame:CGRectMake(130, 70, 50, 20)];
	[num10del setTitle:@"-10" forState:UIControlStateNormal];
	[num10del setTag:15];
	[num10del addTarget:self action:@selector(stringCountChange:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:num10del];
	
	
	
	UIButton * num1add = [UIButton buttonWithType:UIButtonTypeSystem];
	[num1add setFrame:CGRectMake(190, 40, 50, 20)];
	[num1add setTitle:@"+1" forState:UIControlStateNormal];
	[num1add setTag:16];
	[num1add addTarget:self action:@selector(stringCountChange:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:num1add];
	
	UIButton * num1del = [UIButton buttonWithType:UIButtonTypeSystem];
	[num1del setFrame:CGRectMake(190, 70, 50, 20)];
	[num1del setTitle:@"-1" forState:UIControlStateNormal];
	[num1del setTag:17];
	[num1del addTarget:self action:@selector(stringCountChange:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:num1del];
	
	
	UIButton * sendData = [UIButton buttonWithType:UIButtonTypeSystem];
	[sendData setFrame:CGRectMake(250, 40, 50, 20)];
	[sendData setTitle:@"入力！" forState:UIControlStateNormal];
	[sendData addTarget:self action:@selector(sendMessage:) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:sendData];
	
}

-(void)stringCountChange:(UIButton*)tapBuuton {
	switch (tapBuuton.tag) {
		case 10:
			_stringCount += 1000;
			break;
		case 11:
			_stringCount -= 1000;
			break;
		case 12:
			_stringCount += 100;
			break;
		case 13:
			_stringCount -= 100;
			break;
		case 14:
			_stringCount += 10;
			break;
		case 15:
			_stringCount -= 10;
			break;
		case 16:
			_stringCount += 1;
			break;
		case 17:
			_stringCount -= 1;
			break;
		default:
			break;
	}
	_nowStringCountLabel.text = [NSString stringWithFormat:@"%d", _stringCount];
}

-(void)sendMessage:(UIButton*)button {
	for (int i = 0; i < _stringCount; i++) {
		[self.textDocumentProxy insertText:@"あ"];
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated
}

- (void)textWillChange:(id<UITextInput>)textInput {
    // The app is about to change the document's contents. Perform any preparation here.
}

- (void)textDidChange:(id<UITextInput>)textInput {
    // The app has just changed the document's contents, the document context has been updated.
    
    UIColor *textColor = nil;
    if (self.textDocumentProxy.keyboardAppearance == UIKeyboardAppearanceDark) {
        textColor = [UIColor whiteColor];
    } else {
        textColor = [UIColor blackColor];
    }
    [self.nextKeyboardButton setTitleColor:textColor forState:UIControlStateNormal];
}

@end
